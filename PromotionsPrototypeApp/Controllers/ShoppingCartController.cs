﻿using MoreLinq;
using PromotionsPrototypeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PromotionsPrototypeApp.Controllers
{
    public class ShoppingCartController : Controller
    {
        Entities db = new Entities();

        public ActionResult Index()
        {
            if (Session["cart"] == null)
                Session["cart"] = new List<Item>();

            return View("Cart");
        }

        public ActionResult AddProduct(int id)
        {
            List<Item> cart;

            if (Session["cart"] == null)
                cart = new List<Item>();
            else
                cart = (List<Item>)Session["cart"];

            Product product = db.Products.Find(id);
            Item item = new Item(product);

            cart.Add(item);

            UpdateCartDiscounts(cart, item);

            Session["cart"] = cart;

            return View("Cart");
        }

        public ActionResult RemoveProduct(int id)
        {
            List<Item> cart = (List<Item>)Session["cart"];
            Item productItem = cart[cart.FindLastIndex(x => x.Product.ID == id)];

            cart.Remove(productItem);

            UpdateCartDiscounts(cart, productItem);

            Session["cart"] = cart;
            return View("Cart");
        }

        private void UpdateCartDiscounts(List<Item> cart, Item item)
        {
            SpecialOffer specialOffer = item.Product.SpecialOffer;

            if(specialOffer != null)
            {
                var productItems = cart.Where(x => x.Product.ID == item.Product.ID).ToList();
                var itemsBatch = productItems.Batch(specialOffer.ItemsMinimumNumber.Value).ToList();

                foreach (Item[] batchProductItems in itemsBatch)
                {
                    if (batchProductItems.Count() == specialOffer.ItemsMinimumNumber.Value)
                        batchProductItems.ForEach(p => p.SalesPrice = p.Product.Price - (p.Product.Price * (decimal)specialOffer.Discount));
                    else
                        batchProductItems.ForEach(p => p.SalesPrice = p.Product.Price);
                }
            }            
        }
    }
}