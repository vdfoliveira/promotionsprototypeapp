﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PromotionsPrototypeApp.Models;

namespace PromotionsPrototypeApp.Controllers
{
    public class SpecialOfferController : Controller
    {
        private Entities db = new Entities();

        // GET: SpecialOffer
        public ActionResult Index()
        {
            return View(db.SpecialOffers.ToList());
        }

        // GET: SpecialOffer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialOffer SpecialOffer = db.SpecialOffers.Find(id);
            if (SpecialOffer == null)
            {
                return HttpNotFound();
            }
            return View(SpecialOffer);
        }

        // GET: SpecialOffer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SpecialOffers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Discount,ItemsMinimumNumber")] SpecialOffer SpecialOffer)
        {
            if (ModelState.IsValid)
            {
                SpecialOffer.Discount = SpecialOffer.Discount / 100;
                db.SpecialOffers.Add(SpecialOffer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(SpecialOffer);
        }

        // GET: SpecialOffers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialOffer SpecialOffer = db.SpecialOffers.Find(id);
            if (SpecialOffer == null)
            {
                return HttpNotFound();
            }

            SpecialOffer.Discount = SpecialOffer.Discount * 100;
            return View(SpecialOffer);
        }

        // POST: SpecialOffers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Discount,ItemsMinimumNumber")] SpecialOffer SpecialOffer)
        {
            if (ModelState.IsValid)
            {
                SpecialOffer.Discount = SpecialOffer.Discount / 100;
                db.Entry(SpecialOffer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(SpecialOffer);
        }

        // GET: SpecialOffers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecialOffer SpecialOffer = db.SpecialOffers.Find(id);
            if (SpecialOffer == null)
            {
                return HttpNotFound();
            }
            return View(SpecialOffer);
        }

        // POST: SpecialOffers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpecialOffer SpecialOffer = db.SpecialOffers.Find(id);
            db.SpecialOffers.Remove(SpecialOffer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
