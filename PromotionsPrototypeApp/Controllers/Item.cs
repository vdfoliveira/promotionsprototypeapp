﻿using PromotionsPrototypeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PromotionsPrototypeApp.Controllers
{
    public class Item
    {
        private decimal salesPrice;

        public decimal SalesPrice
        {
            get { return salesPrice; }
            set { salesPrice = value; }
        }

        private Product product;

        public Product Product
        {
            get { return product; }
            set { product = value; }
        }

        public Item()
        {

        }

        public Item(Product product)
        {
            this.Product = product;
            this.SalesPrice = product.Price;
        }
    }
}