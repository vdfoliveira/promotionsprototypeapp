# Protótipo para funcionalidade de Promoções



#### Aplicação web escrita em ASP.NET MVC e C#. Para a camada de acesso a dados foi utilizado o Entity Framework 6.0. O banco de dados utilizado foi o SQL Server, hospedado na Azure. Esta aplicação é um protótipo para uma possível funcionalidade de **Promoções**. A aplicação possui telas para gerenciamento de produtos e promoções.

### A aplicação possui as seguintes telas:
- Loja (Tela inicial. Lista os produtos cadastrados e seus respectivos preços e promoções. O botão **Comprar** adiciona um produto ao Carrinho);
- Produtos (Tela com comandos para gerenciar os registros da Entidade **Produto**);
- Promoções (Tela com comandos para gerenciar os registros da Entidade **Promoção**);
- Carrinho (Tela onde são exibidos os produtos comprados, suas quantidades, seus preços e o valor total da compra).



----------



### Para compilar a aplicação:
- Clone este repositório;
- Inicie o Microsoft Visual Studio e no menu clique em **File > Open > Project/Solution**;
- Clique com o botão direito sobre a solução e selecione **Restore Nuget Packages**. Confirme as opções apresentadas;
- No menu, clique em **Build > Build Solution**. Aguarde a compilação ser concluída;
- Novamente no menu, clique em **Debug > Start Debugging** ou pressione em seu teclado o botão F5.




#### Para testar a aplicação sem necessidade de preparação de ambiente local, acesse: http://promotionsprototypeapp.azurewebsites.net/
